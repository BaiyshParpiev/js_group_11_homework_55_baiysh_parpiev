import React from 'react';

const Price = ({priceOfBurgers}) => {
    return (
            <p>Price: {priceOfBurgers}</p>
    );
};

export default Price;