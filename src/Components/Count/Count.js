import React from 'react';

const Count = props => (
    <li>
        <button
            onClick={()=> props.changeCount(props.id)}
        >+</button>
        {props.name}:
        <span>x{props.count}</span>
        <button className='btn-delete' onClick={() => props.changeMinusCount(props.id)}/>
    </li>
)

export default Count;