import {useState} from 'react';
import {nanoid} from 'nanoid'
import './App.css';
import Price from "./Components/Price/Price";
import Ingredients from "./Components/Ingredients/Ingredients";
import Count from "./Components/Count/Count";

const App = () => {
    const ingredients = [
        {name: 'Meat', price: 50},
        {name: 'Cheese', price: 20},
        {name: 'Bacon', price: 30},
        {name: 'Salad', price: 5},
    ];

    const [ingredient, setIngredient] = useState([
        {name: 'Meat', count: 1, id: nanoid()},
        {name: 'Cheese', count: 1, id: nanoid()},
        {name: 'Bacon', count: 1, id:nanoid()},
        {name: 'Salad', count: 1, id: nanoid()}
    ]);

    const burgers = ingredient => {
        const arr = [];
        for(let i = 0; i < ingredient.length; i++){
            for(let j = 0; j < ingredient[i].count; j++){
                arr.push({name: ingredient[i].name, id: nanoid(), })
            }
        }
        return arr
    }

    const priceOfBurgers = () => {
        let sum = 20;
        for(let i = 0; i < ingredients.length; i++){
            const count = burgers(ingredient).filter(task => task.name === ingredients[i].name);
            sum += count.length * ingredients[i].price;
        }
        return sum
    }

    const changeCount = index => {
        setIngredient(prev =>prev.map( p => {
            if(p.id === index){
                return {...p, count: p.count + 1};
            }
            return p;
        }));
        priceOfBurgers(ingredients, burgers(ingredient));
    }
    const changeMinusCount = id => {
        setIngredient(prev =>prev.map(p => {
            if(p.id === id){
                return {...p, count: p.count - 1};
            }
            return p;
        }));
        priceOfBurgers(ingredients, burgers(ingredient));
    }

    const ingredientsOfBurger = burgers(ingredient).map(m => (
        <Ingredients key={m.id} name={m.name}/>
    ));

    const countIngredients = ingredient.map(i => (
        <Count count={i.count}
               name={i.name}
               key={i.id}
               id={i.id}
               changeCount={changeCount}
               changeMinusCount={changeMinusCount}/>
    ))

    return (
        <div className="container">
            <div className="burgerPrice">
                <h1>Ingredients</h1>
                <ul>
                    {countIngredients}
                </ul>
            </div>
            <div className="Burger">
                <h1>Burger</h1>
                <div className="BreadTop">
                    <div className="Seeds1"></div>
                    <div className="Seeds2"></div>
                </div>
                {ingredientsOfBurger}
                <div className="BreadBottom"></div>
                <Price priceOfBurgers={priceOfBurgers()}/>
            </div>
        </div>
    );
}

export default App;
